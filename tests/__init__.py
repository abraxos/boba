from typeguard import typeguard_ignore
from typeguard.importhook import install_import_hook

install_import_hook('boba_fetch')
